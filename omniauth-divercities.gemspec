# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'omniauth/divercities/version'

Gem::Specification.new do |spec|
  spec.name          = 'omniauth-divercities'
  spec.version       = OmniAuth::Divercities::VERSION
  spec.authors       = ['Sébastien Charrier', 'Thomas Parquier']
  spec.email         = ['sebastien@craftsmen.io', 'thomas.parquier@1d-lab.eu']
  spec.summary       = 'Omniauth strategy for Divercities app'
  spec.description   = 'Omniauth strategy for Divercities app'
  spec.homepage      = 'http://gitlab.com/1dlab/omniauth-divercities'
  spec.license       = 'MIT'

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = 'exe'
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ['lib']

  spec.add_runtime_dependency 'omniauth-oauth2', '~> 1.7.3'

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'shoulda-matchers'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'webmock'
end
