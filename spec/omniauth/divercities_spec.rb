describe OmniAuth::Divercities do
  it 'has a version number' do
    expect(OmniAuth::Divercities::VERSION).not_to be nil
  end

  it 'can be loaded by Omniauth' do
    expect do
      OmniAuth::Builder.new(nil) do
        provider :divercities, 'aaa', 'bbb'
      end
    end.to_not raise_error
  end
end
